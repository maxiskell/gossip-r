# -*- coding: utf-8 -*-

from flask_script import Manager
from gossipr import create_app
from gossipr.models import create_tables

app = create_app()
manager = Manager(app)

@manager.command
def init_db():
    create_tables()

if __name__ == '__main__':
    manager.run()

# -*- coding: utf-8 -*-

import json
import gossipr
import unittest

class UserTestCase(unittest.TestCase):
    def setUp(self):
        app = gossipr.create_app(gossipr.config.TestConfig)
        self.app = app.test_client()
        with app.app_context():
            gossipr.models.create_tables()

    def create_user(self, username='cool_user', password='1234'):
        return self.app.post('/users/',
                             data=json.dumps(dict(
                                 username=username,
                                 password=password)),
                             content_type='application/json')
    def test_no_users(self):
        res = self.app.get('/users/')

        assert '[]' in res.data

    def test_all_users(self):
        self.create_user()
        self.create_user('cooler_user')

        res = self.app.get('/users/')

        assert 'cool_user' in res.data
        assert 'cooler_user' in res.data

    def test_get_user(self):
        self.create_user()

        res = self.app.get('/users/1')

        assert 'cool_user' in res.data

    def test_create_user(self):
        res = self.create_user()

        assert '201' in res.status
        assert 'created_at' in res.data

    def test_password_required(self):
        res = self.app.post('/users/',
                           data=json.dumps(dict(username='danny_boy')),
                           content_type='application/json')

        assert '422' in res.status
        assert 'required' in res.data

    def test_password_hashed(self):
        res = self.create_user()

        assert '1234' not in res.data

    def test_username_too_short(self):
        res = self.create_user('short')

        assert '422' in res.status
        assert 'min length is 8' in res.data

    def test_username_too_long(self):
        res = self.create_user('toolongtobeausernameforthisapi')

        assert '422' in res.status
        assert 'max length is 25' in res.data

    def test_username_alphadash(self):
        res = self.create_user('correct_username')

        assert '201' in res.status

    def test_username_alphadash_only(self):
        res = self.create_user('_so$many&special!chars_')

        assert '422' in res.status
        assert 'value does not match regex' in res.data

    def test_user_not_found(self):
        res = self.app.get('/users/1')

        assert '404' in res.status

    def test_user_gone(self):
        self.create_user()
        self.app.delete('/users/1')

        res = self.app.get('/users/1')

        assert '404' in res.status

    def test_update_username(self):
        self.create_user()

        res = self.app.patch('/users/1',
                           data=json.dumps(dict(
                               username='new_username')),
                           content_type='application/json')

        assert '200' in res.status
        assert 'new_username' in res.data

    def test_username_taken(self):
        self.create_user('cool_name')

        res = self.app.post('/users/',
                           data=json.dumps(dict(
                               username='cool_name',
                               password='1234')),
                           content_type='application/json')

        assert '422' in res.status
        assert 'taken' in res.data

    def test_get_user_gossips(self):
        self.create_user()
        self.app.post('/gossips/',
                      data=json.dumps(dict(
                          uid=1,
                          content='hello, gossiprs!')),
                      content_type='application/json')

        res = self.app.get('/users/1/gossips/')

        assert '200' in res.status
        assert 'gossiprs' in res.data

if __name__ == '__main__':
    unittest.main()

# -*- coding: utf-8 -*-

import json
import gossipr
import unittest

class GossipTestCase(unittest.TestCase):
    def setUp(self):
        app = gossipr.create_app(gossipr.config.TestConfig)
        self.app = app.test_client()
        with app.app_context():
            gossipr.models.create_tables()

        self.app.post('/users/',
                      data=json.dumps(dict(
                          username='cool_user',
                          password='1234')),
                      content_type='application/json')

    def create_gossip(self, uid=1, content='hello!'):
        return self.app.post('/gossips/',
                             data=json.dumps(dict(
                                 uid=uid,
                                 content=content)),
                             content_type='application/json')

    def test_no_gossips(self):
        res = self.app.get('/gossips/')

        assert '200' in res.status
        assert '[]' in res.data

    def test_all_gossips(self):
        self.create_gossip()
        self.create_gossip(content='hello, again!')

        res = self.app.get('/gossips/')

        assert 'hello!' in res.data
        assert 'hello, again!' in res.data

    def test_get_gossip(self):
        self.create_gossip()

        res = self.app.get('/gossips/1')

        assert 'hello!' in res.data

    def test_create_gossip(self):
        res = self.create_gossip()

        assert '201' in res.status
        assert 'hello!' in res.data

    def test_invalid_user(self):
        res = self.create_gossip(2)

        assert '422' in res.status
        assert 'Invalid User' in res.data

    def test_content_required(self):
        res = self.app.post('/gossips/',
                            data=json.dumps(dict(
                                uid=1)),
                            content_type='application/json')

        assert '422' in res.status
        assert 'content' in res.data
        assert 'required field' in res.data

    def test_uid_required(self):
        res = self.app.post('/gossips/',
                            data=json.dumps(dict(
                                content='bleh')),
                            content_type='application/json')

        assert '422' in res.status
        assert 'uid' in res.data
        assert 'required field' in res.data

    def test_character_limit(self):
        long_content = ("this is a very"
                        " long string, and if"
                        " I could, I would do it"
                        " even longer, but my fingers"
                        " are really tired from"
                        " all that application program"
                        " interface coding.")

        res = self.create_gossip(content=long_content)

        assert '422' in res.status
        assert 'max length is 141' in res.data

    def test_destroy_gossip(self):
        self.create_gossip()

        res = self.app.delete('/gossips/1')

        assert '200' in res.status
        assert 'deleted' in res.data


# Gossip-r API

The API for a twitter-like app for gossip fans! The twist: 141 characters per gossip
(yeah, one more character of fun!)

It consists in two resources: users and gossips.

## Installation and base configuration

Clone the project

```
$ git clone https://gitlab.com/maxiskell/gossip-r
```

Initiate virtualenv and install dependencies

```
$ virtualenv venv
$ . venv/bin/activate
$ pip install -r requirements.txt
```

Run the manage script to initialize the database

```
$ python manage.py init_db
```

To run the tests

```
$ python tests.py
```

## Run the application

Configure environment variables and run the server

```
$ export FLASK_APP=run.py
$ export FLASK_DEBUG=True # Optional
$ flask run
```

Have fun! :)

---

## About the implementation

I chose Flask, Peewee and SQLite because that little stack let me code a minimum API like this in
a really short period time, and it's also easy to test and try locally on a machine without too much fuzz.

I tried to took the most minimal and understandable approach possible. I know there's plenty of room for
improvements, like implementing migrations (alembric would be a good choice) or an standard JSON API response
format (jsonapi.org would be cool), an authentication system and appropiate responses for soft-deleted users
(410 HTTP Code looks like a really good fit for this one), among others.

I just wanted to expose my knowledge in RESTFul Services, database relations and ORMS and TDD keeping everything
as clean and easy to read as possible.

---

## Users Endpoints

### GET /users/

Retrieve all users in the database.

Example: `{base_url}/users/`

Response:

```
HTTP/1.0 200 OK
Content-Type: application/json

[
    {
        "id": 1,
        "username": "gossip_gal",
        "password": "$2b$12$QeBR5dIfIn0TdqneyZkwMuzSaHkToK8jU0Pu44HZrT1R3KoSLE.Ba",
        "created_at": "2017-09-12 20:39:54.811122",
        "deleted_at": null,
        "updated_at": "2017-09-12 20:39:54.811154",
        "gossips": [
            {
                "content": "hello",
                "created_at": "2017-09-12 22:18:09.587203",
                "id": 1,
                "updated_at": "2017-09-12 22:18:09.587216"
            }
        ]
    },
    {
        "id": 2,
        "username": "cool_boy",
        ...
        "gossips": [
          ...
        ],
    },
]

```

### GET /users/{id}

Retrieve a specific user record, corresponding with the given id

Example: `{base_url}/users/1`

Response:

```
HTTP/1.1 200 OK
Content-Type: application/json

{
    "id": 1,
    "username": "pepepepe",
    "created_at": "2017-09-12 20:39:54.811122",
    "updated_at": "2017-09-12 20:39:54.811154",
    "deleted_at": null,
    "gossips": [
        {
            "content": "hello",
            "created_at": "2017-09-12 22:18:09.587203",
            "id": 1,
            "updated_at": "2017-09-12 22:18:09.587216"
        }
    ]
}


```

Not found response:

```
HTTP/1.1 404 Not Found
Content-Type: application/json

User not found
```

### POST /users/

Create a new user record.

Parameter | Description | Optional
--- | --- | ---
username | _The username for the new user_ | NO
password | _The password for the new user_ | NO

Example: `{base_url}/users/ {"username":"the_teller", "password":"1234"}`

Response:

```
HTTP/1.0 201 CREATED
Content-Type: application/json

{
    "id": 2,
    "username": "the_teller",
    "password": "$2b$12$Ny15wpYkUTcAw6CcN5n0yuXWZJPYumF4NCJj.lbKk6zrrmekHxiDy",
    "created_at": "Tue, 12 Sep 2017 23:01:26 GMT",
    "updated_at": "Tue, 12 Sep 2017 23:01:26 GMT",
    "deleted_at": null
}

```

Validation error response (example):

```
HTTP/1.0 422 UNPROCESSABLE ENTITY
Content-Type: application/json

{
    "password": [
        "required field"
    ]
}

```

### PATCH /users/{id}

Update a user's data.

Parameter | Description | Optional
--- | --- | ---
username | _The new username for the user_ | YES
password | _The new password for the user_ | YES

Example: `{base_url}/users/1 {"username":"tellme_more"}`

Response:

```
HTTP/1.0 200 OK
Content-Type: application/json

{
    "id": 1, 
    "username": "tellme_more"
    "password": "$2b$12$QeBR5dIfIn0TdqneyZkwMuzSaHkToK8jU0Pu44HZrT1R3KoSLE.Ba", 
    "created_at": "2017-09-12 20:39:54.811122", 
    "updated_at": "2017-09-12 20:39:54.811154", 
    "deleted_at": null
}

```

Validation error response (example):

```
HTTP/1.0 422 UNPROCESSABLE ENTITY
Content-Type: application/json

{
    "username": [
        "value does not match regex '^[A-Za-z0-9]*(_*[A-Za-z0-9]*)$'"
    ]
}

```

### DELETE /users/{id}

Remove a user (soft-delete).

Example: `{base_url}/users/2`

Response:

```
HTTP/1.0 200 OK
Content-Type: application/json

{
    "id": 2,
    "username": "the_teller",
    "created_at": "2017-09-12 23:01:26.755419",
    "updated_at": "2017-09-12 23:01:26.755452",
    "deleted_at": "Tue, 12 Sep 2017 23:04:55 GMT",
    "password": "$2b$12$Ny15wpYkUTcAw6CcN5n0yuXWZJPYumF4NCJj.lbKk6zrrmekHxiDy"
}

```

Error response:

```
HTTP/1.1 404 Not Found
Content-Type: application/json

User not found.
```

### GET /users/{id}/gossips/

Retrieve all gossips from a given user.

Example: `{base_url}/users/1/gossips/`

Response:

```
HTTP/1.0 200 OK
Content-Type: application/json

[
    {
        "id": 1,
        "content": "hello",
        "created_at": "2017-09-12 22:18:09.587203",
        "updated_at": "2017-09-12 22:18:09.587216",
        "user": {
            "id": 1,
            "username": "tellme_more"
            "created_at": "2017-09-12 20:39:54.811122",
            "deleted_at": null,
            "updated_at": "2017-09-12 20:39:54.811154"
        }
    },
    {
        "id": 2,
        "content": "hello again!",
        "created_at": "2017-09-12 23:18:09.587203",
        "updated_at": "2017-09-12 23:18:09.587216",
        "user": {
            ...
        }
    }
]
```
Error response:

```
HTTP/1.1 404 Not Found
Content-Type: application/json

User not found.
```

---

## Gossips Endpoints

### GET /gossips/

Retrieve all gossips in the database.

Example: `{base_url}/gossips/`

Response:

```
HTTP/1.0 200 OK
Content-Length: 429
Content-Type: application/json

[
    {
        "id": 1,
        "content": "hello",
        "created_at": "2017-09-12 22:18:09.587203",
        "updated_at": "2017-09-12 22:18:09.587216",
        "user": {
            "id": 1,
            "username": "tellme_more"
            "created_at": "2017-09-12 20:39:54.811122",
            "deleted_at": null,
            "updated_at": "2017-09-12 20:39:54.811154"
        }
    },
    {
        "id": 2,
        "content": "hello again!",
        "created_at": "2017-09-12 23:18:09.587203",
        "updated_at": "2017-09-12 23:18:09.587216",
        "user": {
            ...
        }
    }
]
```

### GET /gosspis/{id}

Retrieve a specific gossip record, corresponding with the given id

Example: `{base_url}/gossips/1`

Response:

```
HTTP/1.0 200 OK
Content-Type: application/json

{
    "id": 1,
    "content": "hello",
    "created_at": "2017-09-12 22:18:09.587203",
    "updated_at": "2017-09-12 22:18:09.587216",
    "user": {
      ...
    }
}
```

Not found response:

```
HTTP/1.1 404 Not Found
Content-Type: application/json

Gossip not found.
```

### POST /gossips/

Create a new gossip.

Parameter | Description | Optional
--- | --- | ---
uid | _The id correspondig to the gossipr (the user posting the gossip)_ | NO
content | _The gossip itself_ | NO

Example: `{base_url}/gossips/ {"uid":1, "content":"hello, gossiprs!"}`

Response:

```
HTTP/1.0 201 CREATED
Content-Type: application/json

{
    "id": 1,
    "content": "Hello, gossiprs!",
    "created_at": "Tue, 12 Sep 2017 23:14:19 GMT",
    "updated_at": "Tue, 12 Sep 2017 23:14:19 GMT",
    "user": {
        "id": 1,
        "username": "tellme_more",
        ...
    }
}
```

Validation error response (example):

```
HTTP/1.0 422 UNPROCESSABLE ENTITY
Content-Type: application/json

{
    "content": [
        "required field"
    ]
}

```

### DELETE /gossips/{id}

Remove a gossip from the database.

Example: `{base_url}/gossips/2`

Response:

```
HTTP/1.0 200 OK
Content-Type: application/json

Gossip successfully deleted.
```

Error response:

```
HTTP/1.1 404 Not Found
Content-Type: application/json

Gossip not found.
```

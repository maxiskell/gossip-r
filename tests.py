# -*- coding: utf-8 -*-

import unittest

from tests.users import UserTestCase
from tests.gossips import GossipTestCase

if __name__ == '__main__':
    unittest.main()

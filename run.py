# -*- coding: utf-8 -*-

from flask import jsonify
from gossipr import create_app

app = create_app()

if __name__ == '__main__':
    app.run()

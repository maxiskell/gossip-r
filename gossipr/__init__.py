# -*- coding: utf-8 -*-

from config import DefaultConfig
from database import db
from flask import Flask, jsonify
from views.users import users
from views.gossips import gossips

def create_app(config=None): 
    app = Flask(__name__)
    init_config(app, config)
    init_db(db, app)
    init_blueprints(app)

    @app.route('/')
    def apiv():
        return jsonify('Gossip-r API v1')

    @app.errorhandler(404)
    def notfound(e):
        return jsonify('Nothing to see here!'), 404

    @app.errorhandler(500)
    def internal_error(e):
        return jsonify('We are sorry, an internal server error occurred.'), 500

    return app

def init_config(app, config=None):
    app.config.from_object(DefaultConfig)

    if config:
        app.config.from_object(config)

def init_db(db, app):
    db.init(app.config['DATABASE'])

def init_blueprints(app):
    app.register_blueprint(users, url_prefix='/users')
    app.register_blueprint(gossips, url_prefix='/gossips')

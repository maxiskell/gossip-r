# -*- coding: utf-8 -*-

from flask import Blueprint, request, jsonify
from peewee import DoesNotExist
from playhouse.shortcuts import model_to_dict
from gossipr.models import Gossip, User
from gossipr.validators import gossip_validator

gossips = Blueprint('gossips', __name__)

@gossips.route('/', methods=['GET'])
def index():
    """ Return all gossips, ordered from newest to oldest. """

    gossips = Gossip.select().order_by(Gossip.created_at.desc())
    return jsonify([model_to_dict(g, backrefs=True) for g in gossips])

@gossips.route('/', methods=['POST'])
def store():
    """ Register a new gossip in the database. """

    status = 201
    data = request.get_json(force=True)

    if gossip_validator.validate(data):
        try:
            user = User.get(User.id == data['uid'])

            gossip = Gossip.create(
                user = user,
                content = data['content'])

            response = model_to_dict(gossip)
        except DoesNotExist:
            response = 'Invalid User id: {}'.format(data['uid'])
            status = 422
    else:
        response = gossip_validator.errors
        status = 422

    return jsonify(response), status

@gossips.route('/<int:id>', methods=['GET'])
def show(id):
    """ Return the gossip data corresponding to a given id. """

    status = 200

    try:
        response = model_to_dict(Gossip.get(Gossip.id == id), backrefs=True)
    except DoesNotExist:
        response = 'Gossip not found'

    return jsonify(response), status

@gossips.route('/<int:id>', methods=['DELETE'])
def destroy(id):
    """ Remove the gossip data from the database. """

    status = 200

    try:
        gossip = Gossip.get(Gossip.id == id)
        gossip.delete_instance()
        response = 'Gossip successfully deleted.'
    except DoesNotExist:
        response = 'Gossip not found'
        status = 404

    return jsonify(response), status

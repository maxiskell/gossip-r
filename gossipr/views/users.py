# -*- coding: utf-8 -*-

from datetime import datetime
from flask import Blueprint, request, jsonify
from flask_bcrypt import generate_password_hash
from gossipr.models import User, Gossip
from gossipr.validators import user_validator, user_update_validator
from peewee import IntegrityError, DoesNotExist
from playhouse.shortcuts import model_to_dict

users = Blueprint('users', __name__)

@users.route('/', methods=['GET'])
def index():
    """ Return all registered (non-deleted) users. """

    users = User.select()
    return jsonify([model_to_dict(u, backrefs=True) for u in users])

@users.route('/', methods=['POST'])
def store():
    """ Register a new user record in the database. """

    status = 201
    data = request.get_json(force=True)

    if user_validator.validate(data):
        try:
            user = User.create(
                username=data['username'],
                password=generate_password_hash(data['password']))

            response = model_to_dict(user)
        except IntegrityError:
            response = 'Username taken'
            status = 422
    else:
        response = user_validator.errors
        status = 422

    return jsonify(response), status

@users.route('/<int:id>', methods=['GET'])
def show(id):
    """ Return the user data and gossips corresponding to a given username. """

    status = 200

    try:
        user = User.get(User.id == id)
        response = model_to_dict(user, backrefs=True, exclude=[User.password])
    except DoesNotExist:
        response = 'User not found'
        status = 404

    return jsonify(response), status

@users.route('/<int:id>', methods=['PATCH'])
def update(id):
    """ Update the user data corresponding to a given id. """

    status = 200

    try:
        user = User.get(User.id == id)
        data = request.get_json(force=True)

        if user_update_validator.validate(data):
            if 'username' in data:
                user.username = data['username']
            if 'password' in data:
                user.password = generate_password_hash(data['password'])

            user.save()
            response = model_to_dict(user)
        else:
            response = user_update_validator.errors
            status = 422
    except DoesNotExist:
        response = 'User not found'
        status = 404

    return jsonify(response), status

@users.route('/<int:id>', methods=['DELETE'])
def destroy(id):
    """ Remove (soft-delete) the user data from the database. """

    status = 200

    try:
        user = User.get(User.id == id)
        user.deleted_at = datetime.now()
        user.save()

        response = model_to_dict(user)
    except DoesNotExist:
        response =  'User not found'
        status = 404

    return jsonify(response), status

@users.route('/<int:uid>/gossips/', methods=['GET'])
def gossips(uid):
    """ Return all the gossips from a user, ordered from newest to oldest. """

    status = 200

    try:
        user = User.get(User.id == uid)

        gossips = (Gossip
                   .select()
                   .join(User, on=Gossip.user)
                   .where(Gossip.user == user)
                   .order_by(Gossip.created_at.desc()))

        response = [model_to_dict(s, backrefs=True) for s in gossips]
    except DoesNotExist:
        response = 'User not found'
        status = 404

    return jsonify(response), status

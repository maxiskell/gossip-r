# -*- coding: utf-8 -*-

class DefaultConfig():
    DATABASE = 'gossipr.db'

class TestConfig():
    TESTING = True
    DATABASE = ':memory:'

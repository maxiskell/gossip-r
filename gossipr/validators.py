# -*- coding: utf-8 -*-

from cerberus import Validator

user_validator = Validator({
    'username': {
        'type': 'string',
        'regex': '^[A-Za-z0-9]*(_*[A-Za-z0-9]*)$',
        'required': True,
        'minlength': 8,
        'maxlength': 25
    },
    'password': {
        'required': True
    }
})

user_update_validator = Validator({
    'username': {
        'type': 'string',
        'regex': '^[A-Za-z0-9]*(_*[A-Za-z0-9]*)$',
        'minlength': 8,
        'maxlength': 25
    }
})

gossip_validator = Validator({
    'uid': {
        'type': 'integer',
        'required': True
    },
    'content': {
        'type': 'string',
        'required': True,
        'minlength': 1,
        'maxlength': 141
    }
})

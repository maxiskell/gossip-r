# -*- coding: utf-8 -*-

from database import db
from datetime import datetime
from peewee import *

class BaseModel(Model):
    created_at = DateTimeField(default=datetime.now, formats='%Y-%m-%d')
    updated_at = DateTimeField(default=datetime.now, formats='%Y-%m-%d')

    class Meta:
        database = db

class SoftDeletableModel(BaseModel):
    @classmethod
    def select(cls, *selection):
        query = super(SoftDeletableModel, cls).select(*selection)
        return query.where(cls.deleted_at.is_null())

class User(SoftDeletableModel):
    username = CharField(unique=True, max_length=25)
    password = CharField()
    deleted_at = DateTimeField(null=True, default=None, formats='%Y-%m-%d')

class Gossip(BaseModel):
    user = ForeignKeyField(User, related_name='gossips')
    content = CharField(max_length=141)

def create_tables():
    try:
        User.create_table()
    except OperationalError:
        print 'Users table already exists!'

    try:
        Gossip.create_table()
    except OperationalError:
        print 'Gossips table already exists!'
